package id.binar.learn.taskchapter

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.binar.learn.taskchapter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    /**
     * Portrait to Landscape
     * - onPause
     * - onStop
     * - onDestroy
     * - onCreate
     * - onStart
     * - onResume
     */

    /**
     * Landscape to Portrait
     * - onPause
     * - onStop
     * - onDestroy
     * - onCreate
     * - onStart
     * - onResume
     */

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onCreate")
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onStart")
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onRestart")
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show()
        Log.d("Activity Lifecycle", "onDestroy")
    }
}